from django.contrib import admin
from .models import Announcements,Notices,Marque,Department,Faculty,SliderImage,Result
from .forms import SliderForm,FacultyForm
# Register your models here.
class ImageForm(admin.ModelAdmin):
	form = SliderForm

class FacultyForm(admin.ModelAdmin):
	form = FacultyForm

admin.site.register(Announcements)
admin.site.register(Notices)
admin.site.register(Result)
admin.site.register(Marque)
admin.site.register(Department)
admin.site.register(Faculty,FacultyForm)

admin.site.register(SliderImage,ImageForm)



		