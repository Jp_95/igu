from django.apps import AppConfig


class IguSiteConfig(AppConfig):
    name = 'igu_site'
