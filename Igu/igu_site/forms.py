from django.core.files.images import get_image_dimensions
from django import forms 

from .models import SliderImage,Faculty

class SliderForm(forms.ModelForm):
	class Meta:
		model = SliderImage
		fields = ['image']

	def clean_image(self):
		image = self.cleaned_data.get("image")
		if not image:
			raise forms.ValidationError("No image!")
		else:
			w, h = get_image_dimensions(image)
			if w != 933:
				raise forms.ValidationError("The image is %i pixel wide. It's supposed to be 933px" % w)
			if h != 391:
				raise forms.ValidationError("The image is %i pixel high. It's supposed to be 391px" % h)
		return image


class FacultyForm(forms.ModelForm):
	class Meta:
		model = Faculty
		fields = ['name','pic','designation','qualification','specialisation','experience','email_id','contact','detail_link','department']
	def clean_pic(self):
		image = self.cleaned_data.get("pic")
		if not image:
			raise forms.ValidationError("No image!")
		else:
			w, h = get_image_dimensions(image)
			if (w != 200 or h != 200):
				raise forms.ValidationError(("The image is %i pixel wide and %i pixel high . It's supposed to be 200 X 200px") % (w , h))
			# if h != 391:
			# 	raise forms.ValidationError("The image is %i pixel high. It's supposed to be 391px" % h)
		return image
