import os
from django.db import models
from django.conf import settings
from datetime import date
from PIL import Image
# from django.conf.settings import MEDIA_ROOT
# Create your models here.
def faculty_upload_to(self, filename):
	# title = self.title
	# slug = slugify(title)
	file_extension = filename.split(".")[1]
	new_filename = "%s.%s" % (self.name ,file_extension)
	return "Faculty/%s" % (new_filename)

# def slider_image_upload(self, filename):
# 	# title = self.title
# 	# slug = slugify(title)
# 	file_extension = filename.split(".")[1]
# 	new_filename = "%s.%s" % (self.name ,file_extension)
# 	return "Slider/%s" % (new_filename)


class Announcements(models.Model):
	title = models.CharField(max_length=75)
	link  = models.CharField(max_length=150, blank=True,null=True)
	pic   = models.FileField(verbose_name='Announcement', upload_to='Announcement',null=True,blank=True)
	text  = models.TextField(blank=True,null=True)
	showFrom = models.DateField(default=date.today,editable=True)
	showUpto = models.DateField()
	timestamp = models.DateField(auto_now_add=True,auto_now=False)
	updated = models.DateField(auto_now_add=False,auto_now=True)

	def __str__(self):
		return str(self.title) + ' ('+ str(self.id) + ') ' + str(self.showUpto)

class Result(models.Model):
	title = models.CharField(max_length=75)
	link  = models.CharField(max_length=150, blank=True,null=True)
	pic   = models.FileField(verbose_name='Result', upload_to='Result',null=True,blank=True)
	text  = models.TextField(blank=True,null=True)
	showFrom = models.DateField(default=date.today,editable=True)
	showUpto = models.DateField()
	timestamp = models.DateField(auto_now_add=True,auto_now=False)
	updated = models.DateField(auto_now_add=False,auto_now=True)

	def __str__(self):
		return str(self.title) + ' ('+ str(self.id) + ') ' + str(self.showUpto)


class Marque(models.Model):
	title = models.CharField(max_length=75)
	link  = models.CharField(max_length=150, blank=True,null=True)
	showFrom = models.DateField(default=date.today,editable=True)
	showUpto = models.DateField()
	timestamp = models.DateField(auto_now_add=True,auto_now=False)
	updated = models.DateField(auto_now_add=False,auto_now=True)

	def __str__(self):
		return str(self.title) + ' ('+ str(self.id) + ') ' + str(self.showUpto)


class Notices(models.Model):
	title = models.CharField(max_length=75)
	link  = models.CharField(max_length=150, blank=True,null=True)
	pic   = models.FileField(verbose_name='Notices', upload_to='Notices',null=True,blank=True)
	text  = models.TextField(blank=True,null=True)
	showFrom = models.DateField(default=date.today,editable=True)
	showUpto = models.DateField()
	timestamp = models.DateField(auto_now_add=True,auto_now=False)
	updated = models.DateField(auto_now_add=False,auto_now=True)

	def __str__(self):
		return str(self.title) + ' ('+ str(self.id) + ') ' + str(self.showUpto)

class SliderImage(models.Model):
	image = models.ImageField(verbose_name="sliderImage", 
		upload_to='sliderImage',
		height_field="height_field",
		width_field="width_field")
	height_field = models.PositiveIntegerField(default=933)
	width_field = models.PositiveIntegerField(default=391)
	
	def __str__(self):
		return 'Image(933X391)' + str(self.id)

	
	# def save(self,*args,**kwargs):
	# 	image = (self.image).resize(width=933,height=391)
	# # 	super().save(*args,**kwargs)
	# def save(self, size=(400, 300)):
	# 	"""
	# 	Save Photo after ensuring it is not blank.  Resize as needed.
	# 	"""

	# 	filename = self.image.path
	# 	print(filename)
	# 	image = Image.open(filename)

	# 	image.thumbnail(size, Image.ANTIALIAS)
	# 	image.save(filename)	

class Department(models.Model):
	name = models.CharField(max_length=75)
	
	def __str__(self):
		return self.name
	# courses = models.TextField()

class Faculty(models.Model):
	name = models.CharField(max_length=75)
	pic   = models.ImageField(verbose_name='Profile Pic', 
		upload_to=faculty_upload_to,
		null=True,blank=True,
		)
	designation = models.CharField(max_length=75, blank=True,null=True)
	qualification = models.CharField(max_length=75, blank=True,null=True)
	specialisation = models.CharField(max_length=75, blank=True,null=True)
	experience = models.CharField(max_length=75, blank=True,null=True)
	email_id = models.EmailField(max_length=75, blank=True,null=True)
	detail_link = models.CharField(max_length=100, blank=True,null=True)
	department = models.ForeignKey(Department,on_delete=models.CASCADE)
	contact = models.CharField(max_length=15,blank=True,null=True)
	def __str__(self):
		return str(self.name) + '(' + str(self.designation) + ') - ' +str(self.department)  