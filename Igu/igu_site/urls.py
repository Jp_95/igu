"""Igu URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$',views.index, name="Index" ),
    # about/
    url(r'^about/$',views.BaseView.as_view(), name="about" ),
    url(r'^visionmission/$',views.BaseView.as_view(template_name='VisionMission.html'), name="visionmission" ),

    # administration/
    url(r'^administration/chancellor$',views.BaseView.as_view(template_name='Chancellor.html'), name="chancellor" ),
    url(r'^administration/coe$',views.BaseView.as_view(template_name='COE.html'), name="coe" ),
    url(r'^administration/dean-academics$',views.BaseView.as_view(template_name='DeanAcad.html'), name="deanacad" ),
    url(r'^administration/finance-officer$',views.BaseView.as_view(template_name='Fo.html'), name="fo" ),
    url(r'^administration/registrar$',views.BaseView.as_view(template_name='Registrar.html'), name="registrar" ),
    url(r'^administration/vice-chancellor$',views.BaseView.as_view(template_name='ViceChancellor.html'), name="vicechancellor" ),

    # Cells/
    url(r'^Cells/Cell$',views.BaseView.as_view(template_name='Cell.html'), name="cell" ),
    url(r'^Cells/sc$',views.BaseView.as_view(template_name='SC.html'), name="sc" ),
    url(r'^Cells/tnp$',views.BaseView.as_view(template_name='tnp.html'), name="tnp" ),
    url(r'^Cells/women$',views.BaseView.as_view(template_name='women.html'), name="women" ),

    #Examination
    url(r'^examination/results$',views.BaseView.as_view(template_name='results.html'), name="results" ),
    url(r'^examination/schedules$',views.BaseView.as_view(template_name='Schedules.html'), name="schedules" ),

    #Sidemenu
    url(r'^admission$',views.BaseView.as_view(template_name='Admission.html'), name="admission" ),
    url(r'^admission-form$',views.BaseView.as_view(template_name='AdmissionForm.html'), name="admission-form" ),
    url(r'^anti-ragging$',views.BaseView.as_view(template_name='AntiRagging.html'), name="anti-ragging" ),
    url(r'^ddu$',views.BaseView.as_view(template_name='DDU.html'), name="ddu" ),
    url(r'^download-admit-card$',views.BaseView.as_view(template_name='DownloadAdmitCard.html'), name="download-admit-card" ),
    url(r'^facilities$',views.BaseView.as_view(template_name='Facilities.html'), name="facilities" ),
    url(r'^gian$',views.BaseView.as_view(template_name='GIAN.html'), name="gian" ),
    url(r'^iqac$',views.BaseView.as_view(template_name='IQAC.html'), name="iqac" ),
    url(r'^student-corner$',views.BaseView.as_view(template_name='StudentCorner.html'), name="student-corner" ),
    
    # Departments
    url(r'^department/botany$',views.BaseView.as_view(template_name='DeptBotany.html'), name="botany" ),
    url(r'^department/computer-applications$',views.BaseView.as_view(template_name='DeptCA.html'), name="computer-applications" ),
    url(r'^department/chemistry$',views.BaseView.as_view(template_name='DeptChem.html'), name="chemistry" ),
    url(r'^department/civil$',views.BaseView.as_view(template_name='DeptCivil.html'), name="civil" ),
    url(r'^department/commerce$',views.BaseView.as_view(template_name='DeptCommerce.html'), name="commerce" ),
    url(r'^department/cse$',views.BaseView.as_view(template_name='DeptCSE.html'), name="cse" ),
    url(r'^department/economics$',views.BaseView.as_view(template_name='DeptEco.html'), name="economics" ),
    url(r'^department/english$',views.BaseView.as_view(template_name='DeptEnglish.html'), name="english" ),
    url(r'^department/geography$',views.BaseView.as_view(template_name='DeptGeography.html'), name="geography" ),
    url(r'^department/hindi$',views.BaseView.as_view(template_name='DeptHindi.html'), name="hindi" ),
    url(r'^department/history$',views.BaseView.as_view(template_name='DeptHistory.html'), name="history" ),
    url(r'^department/journalism$',views.BaseView.as_view(template_name='DeptJourno.html'), name="journalism" ),
    url(r'^department/law$',views.BaseView.as_view(template_name='DeptLaw.html'), name="law" ),
    url(r'^department/mathematics$',views.BaseView.as_view(template_name='DeptaMathematics.html'), name="mathematics" ),
    url(r'^department/mechanical$',views.BaseView.as_view(template_name='DeptMech.html'), name="mechanical" ),
    url(r'^department/physics$',views.BaseView.as_view(template_name='DeptPhy.html'), name="physics" ),
    url(r'^department/political-science$',views.BaseView.as_view(template_name='DeptPol.html'), name="political-science" ),
    url(r'^department/printing-technology$',views.BaseView.as_view(template_name='DeptPrint.html'), name="printing-technology" ),
    url(r'^department/sanskrit$',views.BaseView.as_view(template_name='DeptSans.html'), name="sanskrit" ),
    url(r'^department/sociology$',views.BaseView.as_view(template_name='DeptSociology.html'), name="sociology" ),
    url(r'^department/zoology$',views.BaseView.as_view(template_name='DeptZoo.html'), name="zoology" ),

    #
    url(r'^Contact$',views.BaseView.as_view(template_name='Contact.html'), name="contact" ),
    url(r'^library$',views.BaseView.as_view(template_name='Library.html'), name="library" ),





    # url(r'^$',views.index, name="Index" ),
]
