import datetime
from django.shortcuts import render,render_to_response,redirect, get_object_or_404
from django.shortcuts import render
from django.views.generic.base import TemplateView
from .models import Announcements,Notices,Marque,SliderImage,Faculty,Department,Result
from django.db.models import F
# Create your views here.
def index(request):
	context = {}
	context['announcements'] = Announcements.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
	context['notices'] = Notices.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
	context['marques'] = Marque.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
	context['results'] = Result.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
	context['slider']  = SliderImage.objects.all()	
	
	return render(request,"index.html",context)

class BaseView(TemplateView):
	template_name = 'AboutIGU.html'

	def get_context_data(self,*args,**kwargs):
		context = super().get_context_data(*args,**kwargs)
		context['announcements'] = Announcements.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
		context['notices'] = Notices.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
		context['marques'] = Marque.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
		context['results'] = Result.objects.filter(timestamp__lte=F('showUpto')).filter(timestamp__gte=F('showFrom'))
		
		context['faculty_botany'] = Faculty.objects.filter(department=Department.objects.get(name='Botany'))
		context['faculty_ca'] = Faculty.objects.filter(department=Department.objects.get(name='Computer Applications'))
		context['faculty_chem'] = Faculty.objects.filter(department=Department.objects.get(name='Chemistry'))
		context['faculty_civil'] = Faculty.objects.filter(department=Department.objects.get(name='Civil'))
		context['faculty_comm'] = Faculty.objects.filter(department=Department.objects.get(name='Commerce'))
		context['faculty_cse'] = Faculty.objects.filter(department=Department.objects.get(name='Computer Science and Engineering'))
		context['faculty_eco'] = Faculty.objects.filter(department=Department.objects.get(name='Economics'))
		context['faculty_eng'] = Faculty.objects.filter(department=Department.objects.get(name='English'))
		context['faculty_geo'] = Faculty.objects.filter(department=Department.objects.get(name='Geography'))
		context['faculty_hin'] = Faculty.objects.filter(department=Department.objects.get(name='Hindi'))
		context['faculty_his'] = Faculty.objects.filter(department=Department.objects.get(name='History'))
		context['faculty_journo'] = Faculty.objects.filter(department=Department.objects.get(name='Journalism & Mass Comm'))
		context['faculty_law'] = Faculty.objects.filter(department=Department.objects.get(name='Law'))
		context['faculty_maths'] = Faculty.objects.filter(department=Department.objects.get(name='Mathematics'))
		context['faculty_mech'] = Faculty.objects.filter(department=Department.objects.get(name='Mechanical'))
		context['faculty_phy'] = Faculty.objects.filter(department=Department.objects.get(name='Physics'))
		context['faculty_pol'] = Faculty.objects.filter(department=Department.objects.get(name='Political Sciences'))
		context['faculty_print'] = Faculty.objects.filter(department=Department.objects.get(name='Printing and Tech'))
		context['faculty_sans'] = Faculty.objects.filter(department=Department.objects.get(name='Sanskrit'))
		context['faculty_socio'] = Faculty.objects.filter(department=Department.objects.get(name='Sociology'))
		context['faculty_zoo'] = Faculty.objects.filter(department=Department.objects.get(name='Zoology'))
		return context
